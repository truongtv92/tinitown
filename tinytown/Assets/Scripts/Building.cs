using DefaultNamespace;
using UnityEngine;
using UnityEngine.EventSystems;

public class Building : MonoBehaviour,IPointerClickHandler
{
    public BuildingType baseBuildingType;
    public BuildingType currentBuildingType;
    public GameObject baseSet;
    public int x, y;
    private bool isSetBuilding;
    private SpriteRenderer sprite;

    private void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();
    }

    public void Init(BuildingType buildingType, int x, int y)
    {
        baseBuildingType = buildingType;
        currentBuildingType = baseBuildingType;
        SetUI();
        switch (currentBuildingType)
        {
            case BuildingType.None:
            case BuildingType.Land:
                break;
            case BuildingType.Park:
            case BuildingType.Electric:
            case BuildingType.House:
            case BuildingType.Garbage:
            case BuildingType.Road:
                baseSet.SetActive(true);
                break;
        }

        this.x = x;
        this.y = y;
    }

    private void SetUI()
    {
        switch (currentBuildingType)
        {
            case BuildingType.None:
                sprite.color = Color.clear;
                break;
            case BuildingType.Land:
                sprite.color = Color.cyan;
                break;
            case BuildingType.Park:
                sprite.color = Color.green;
                break;
            case BuildingType.Electric:
                sprite.color = Color.blue;
                break;
            case BuildingType.House:
                sprite.color = Color.red;
                break;
            case BuildingType.Garbage:
                sprite.color = Color.yellow;
                    
                break;
            case BuildingType.Road:
                sprite.color = Color.white;
                break;
        }
    }

    private void SetBuildingType(BuildingType buildingType)
    {
        if(baseBuildingType !=BuildingType.Land) return;
        Debug.Log("currentBuildingType "+currentBuildingType + " SetBuildingType "+ buildingType);
        currentBuildingType = buildingType;
        SetUI();
            
    }

    private void Reset()
    
    {
        Debug.Log("currentBuildingType "+currentBuildingType + " vs "+ "baseBuildingType "+baseBuildingType);
        SetBuildingType(baseBuildingType);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (GamePlayController.Instance != null)
        {
            if (currentBuildingType != baseBuildingType) Reset();
            else
            SetBuildingType(GamePlayController.Instance.currentBuilding);
            GamePlayController.Instance.CheckAllMap(this);
        }
        else
        {
            if (MapGenerator.Instance == null) return;
            SetBuildingType(MapGenerator.Instance.currentBuilding);
            MapGenerator.Instance.ChangeData(x,y,(int)currentBuildingType);
        }
    }
}