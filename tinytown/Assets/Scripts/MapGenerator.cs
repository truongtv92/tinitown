﻿using System;
using System.IO;
using DefaultNamespace;
using TMPro;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public BuildingType currentBuilding;
    public Building squarePrefabs;
    public Transform mapContainer;
    public TMP_InputField widthInput, heightInput, houseInput, happyInput;
    private const float SquareWidth = 1f;
    public static MapGenerator Instance { get; private set; }
    private int x = 7, y = 7;
    private int[][] map;

    private void Awake()
    {
        Instance = this;
        widthInput.onValueChanged.AddListener(delegate { OnWidthValueChange(widthInput.text); });
        heightInput.onValueChanged.AddListener(delegate { OnGHeightValueChange(heightInput.text); });
        DrawMap();
        string t = "!#1#2#4#65#";
        var s = t.Split('#');
        foreach (var VARIABLE in s)
        {
            Debug.Log(VARIABLE);
        }
    }


    private void OnWidthValueChange(string value)
    {
        if (value.Trim().Equals(string.Empty))
        {
            widthInput.text = "0";
        }

        if (int.Parse(value.Trim()) > 6)
        {
            widthInput.text = "6";
        }

        x = int.Parse(widthInput.text.Trim());
        DrawMap();
    }

    private void OnGHeightValueChange(string value)
    {
        if (value.Trim().Equals(string.Empty))
        {
            heightInput.text = "0";
        }

        if (int.Parse(value.Trim()) > 6)
        {
            heightInput.text = "6";
        }

        y = int.Parse(heightInput.text.Trim());
        DrawMap();
    }

    private void DrawMap()
    {
        foreach (Transform child in mapContainer)
        {
            Destroy(child.gameObject);
        }

        map = new int[x][];
        for (var i = 0; i < x; i++)
        {
            int[] t = new int[y];
            for (var j = 0; j < y; j++)
            {
                var builder = Instantiate(squarePrefabs, mapContainer, true);
                builder.Init((BuildingType) 1, i, j);
                builder.transform.position =
                    new Vector3(
                        SquareWidth * 0.5f + SquareWidth * (i - x) + SquareWidth * x / 2,
                        SquareWidth * (-1 - j) + SquareWidth * y / 2 + 1, 0);
                t[j] = 1;
            }

            map[i] = t;
        }
    }

    public void ChangeData(int x, int y, int value)
    {
        map[x][y] = value;
    }

    public void SaveToFile()
    {
        var mapData = "";
        for (var j = 0; j < y; j++)
        {
            for (var i = 0; i < x; i++)
            {
                if (i < x - 1)
                    mapData += map[i][j] + ",";
                else
                    mapData += map[i][j] + "#";
            }
        }

        mapData = mapData.Remove(mapData.Length - 1, 1);
        var house = 0;
        var happy = 0;
        if (houseInput.text.Trim().Equals(string.Empty))
        {
            house = 0;
        }
        else
        {
            house = int.Parse(houseInput.text.Trim());
        }
        if (happyInput.text.Trim().Equals(string.Empty))
        {
            happy = 0;
        }
        else
        {
            happy = int.Parse(happyInput.text.Trim());
        }
        var data = new MapLevel(mapData,house,happy);
        string result = JsonUtility.ToJson(data);
        var dataPath = Application.dataPath+"/Resources/Map/";
        int fileCount = Directory.GetFiles(dataPath, "*.txt", SearchOption.AllDirectories).Length+1;
        File.WriteAllText(dataPath+fileCount+".txt",result);
        Debug.Log("WriteAllText " + dataPath + fileCount + ".txt");
    }
}