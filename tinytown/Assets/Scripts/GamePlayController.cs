using System;
using System.Collections.Generic;
using DefaultNamespace;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GamePlayController : MonoBehaviour
{
    public BuildingType currentBuilding;
    public Building squarePrefabs;
    public Transform mapContainer;
    public TextMeshProUGUI houseTxt, happyTxt, roadConnectTxt, buildingConnectTxt, garbageTxt, electricTxt;
    public Toggle houseToogle, happyToogle, roadToogle, buildingToogle, garbageToogle, electricToogle;
    public GameObject winObj;
    private readonly List<Building> _houseList = new List<Building>();
    private readonly List<Building> _garbageList = new List<Building>();
    private readonly List<Building> _electricList = new List<Building>();
    private readonly List<Building> _roadList = new List<Building>();
    private readonly List<Building> _parkList = new List<Building>();
    private const float SquareWidth = 1f;
    private MapLevel _mapLevel;
    public static GamePlayController Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }

    public void Win()
    {
        winObj.SetActive(true);
    }
    public void NextLevel()
    {
        if (Config.MapLevel < Config.totalMap)
        {
            Config.MapLevel++;
            SceneManager.LoadScene("gameplay");
        }
    }
    private void ConvertDataToMap(IReadOnlyList<string[]> data)
    {
        var map = new Building[data.Count][];
        for (var index = 0; index < data.Count; index++)
        {
            var value = data[index];
            var building = new Building[value.Length];
            for (var i = 0; i < value.Length; i++)
            {
                var valueData = value[i];
                var builder = Instantiate(squarePrefabs, mapContainer, true);
                builder.Init((BuildingType) int.Parse(valueData), i, index);
                building[i] = builder;
                builder.transform.position =
                    new Vector3(
                        SquareWidth * 0.5f + SquareWidth * (i - value.Length) + SquareWidth * value.Length / 2,
                        SquareWidth * (-1 - index) + SquareWidth * data.Count / 2 + 1, 0);
                switch (builder.currentBuildingType)
                {
                    case BuildingType.Electric:
                        _electricList.Add(builder);
                        break;
                    case BuildingType.Garbage:
                        _garbageList.Add(builder);
                        break;
                    case BuildingType.House:
                        _houseList.Add(builder);
                        break;
                    case BuildingType.Park:
                        _parkList.Add(builder);
                        break;
                    case BuildingType.Road:
                        _roadList.Add(builder);
                        break;
                    case BuildingType.None:
                        break;
                    case BuildingType.Land:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            map[index] = building;
        }
       
    }

    public void Start()
    {
        var mapData = (TextAsset) Resources.Load("Map/" + Config.MapLevel, typeof(TextAsset));
        _mapLevel = JsonUtility.FromJson<MapLevel>(mapData.text);
        var rows = _mapLevel.mapData.Split('#');
        var data = new string[rows.Length][];
        for (var i = 0; i < rows.Length; i++)
        {
            data[i] = rows[i].Split(',');
        }

        ConvertDataToMap(data);
        SetTxt();
    }

    public void CheckAllMap(Building building)
    {
        _houseList.Remove(building);
        _garbageList.Remove(building);
        _electricList.Remove(building);
        _parkList.Remove(building);
        _roadList.Remove(building);

        switch (building.currentBuildingType)
        {
            case BuildingType.Park:
                _parkList.Add(building);
                break;
            case BuildingType.Electric:
                _electricList.Add(building);
                break;
            case BuildingType.House:
                _houseList.Add(building);
                break;
            case BuildingType.Garbage:
                _garbageList.Add(building);

                break;
            case BuildingType.Road:
                _roadList.Add(building);
                break;
            case BuildingType.None:
                break;
            case BuildingType.Land:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        SetTxt();
    }

    private void SetTxt()
    {
        houseTxt.text = "Houses: " + _houseList.Count + "/" + _mapLevel.houseNumber;
        happyTxt.text = "Happiness: " + CountHappiness() + "/" + _mapLevel.happinessNumber;
        roadConnectTxt.text = "Road connected: ";
        garbageTxt.text = "Garbage station: ";
        buildingConnectTxt.text = "Building by road: ";
        electricTxt.text = "Electric plant:";
        
        houseToogle.isOn = _houseList.Count >= _mapLevel.houseNumber;
        happyToogle.isOn = CountHappiness() >=_mapLevel.happinessNumber;
        roadToogle.isOn = IsRoadConnected();
        garbageToogle.isOn = _garbageList.Count > 0;
        buildingToogle.isOn = IsBuildingConnectToRoad();
        electricToogle.isOn = _electricList.Count > 0;
        if (houseToogle.isOn && happyToogle.isOn && roadToogle.isOn && garbageToogle.isOn && buildingToogle.isOn &&
            electricToogle.isOn)
        {
            Win();
        }
    }
    private bool IsRoadConnected()
    {
        if (_roadList.Count <= 1) return false;
        foreach (var t in _roadList)
        {
            if (_roadList.Find(a => a.x == t.x + 1 && a.y == t.y) == null &&
                _roadList.Find(a => a.x == t.x - 1 && a.y == t.y) == null &&
                _roadList.Find(a => a.y == t.y + 1 && a.x == t.x) == null &&
                _roadList.Find(a => a.y == t.y - 1 && a.x == t.x) == null) return false;
        }

        return true;
    }


    private bool IsBuildingConnectToRoad()
    {
        if (_houseList.Count < 1 && _electricList.Count < 1 && _garbageList.Count < 1) return false;
        foreach (var t in _houseList)
        {
            if (_roadList.Find(a => a.x == t.x + 1 && a.y == t.y) == null &&
                _roadList.Find(a => a.x == t.x - 1 && a.y == t.y) == null &&
                _roadList.Find(a => a.y == t.y + 1 && a.x == t.x) == null &&
                _roadList.Find(a => a.y == t.y - 1 && a.x == t.x) == null) return false;
        }

        foreach (var t in _electricList)
        {
            if (_roadList.Find(a => a.x == t.x + 1 && a.y == t.y) == null &&
                _roadList.Find(a => a.x == t.x - 1 && a.y == t.y) == null &&
                _roadList.Find(a => a.y == t.y + 1 && a.x == t.x) == null &&
                _roadList.Find(a => a.y == t.y - 1 && a.x == t.x) == null) return false;
        }

        foreach (var t in _garbageList)
        {
            if (_roadList.Find(a => a.x == t.x + 1 && a.y == t.y) == null &&
                _roadList.Find(a => a.x == t.x - 1 && a.y == t.y) == null &&
                _roadList.Find(a => a.y == t.y + 1 && a.x == t.x) == null &&
                _roadList.Find(a => a.y == t.y - 1 && a.x == t.x) == null) return false;
        }


        return true;
    }

    private int CountHappiness()
    {
        var result = 0;
        result += _houseList.Count;
        foreach (var house in _houseList)
        {
           foreach(var electric in _electricList)
            {
                if(Mathf.Abs(electric.x - house.x) <= 2 && Mathf.Abs(electric.y - house.y) <= 2)
                {
                    result--;
                }
            }

            foreach (var garbarge in _garbageList)
            {
                if (Mathf.Abs(garbarge.x - house.x) <= 2 && Mathf.Abs(garbarge.y - house.y) <= 2)
                {
                    result--;
                }
            }

            if (_parkList.Find(a => a.x == house.x + 1 && a.y == house.y) != null) result++;
            if (_parkList.Find(a => a.x == house.x - 1 && a.y == house.y) != null) result++;
            if (_parkList.Find(a => a.y == house.y + 1 && a.x == house.x) != null) result++;
            if (_parkList.Find(a => a.y == house.y - 1 && a.x == house.x) != null) result++;
        }

        return result;
    }
}