namespace DefaultNamespace
{
    public class MapLevel
    {
        public string mapData;
        public int houseNumber;
        public int happinessNumber;

        public MapLevel(string mapData, int houseNumber, int happinessNumber)
        {
            this.mapData = mapData;
            this.houseNumber = houseNumber;
            this.happinessNumber = happinessNumber;
        }
    }
}