using UnityEngine;

namespace DefaultNamespace
{
    public class BuildingButton : MonoBehaviour
    {
        public BuildingType type;

        public void Onclick()
        {
            if (GamePlayController.Instance != null)
            {
                GamePlayController.Instance.currentBuilding = type;
                return;
            }

            if (MapGenerator.Instance != null)
            {
                MapGenerator.Instance.currentBuilding = type;
                return;
            }
        }
    }
}